;;; esp.el --- Useful shortcuts to work with the ESP32 board  -*- lexical-binding: t; -*-

;; Copyright (C) 2019  Juan Francisco Giménez Silva

;; Author: Juan Francisco Giménez Silva <juanfgs@gmail.com>
;; Keywords: hardware, convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 
;;; Code:
(defgroup esp '((string  custom-variable))
  "Customization group for esp-mode."
  :group 'emacs
  :version 0.2)
(defcustom esp-mode-entrypoint nil
  "Define the script that will be run by default when calling ampy-run."
  :type '(string)
  :group 'esp)
(defcustom esp-mode-device "/dev/ttyUSB0"
  "Set the device file that wil be used to connect ampy."
  :type '(string)
  :group 'esp
  )
(defun get-relative-path (buffer-path)
  "Remove the root project path to get the relative BUFFER-PATH."
  (replace-regexp-in-string (projectile-project-root) ""
                            (expand-file-name buffer-path))
  )
(defun ampy-put-current-buffer ()
  "Execute ampy put command."
  (interactive)
  (shell-command (concat "ampy -p " esp-mode-device " put " buffer-file-truename
                         " " (get-relative-path buffer-file-truename)))
  )
(defun ampy-rm-current-buffer ()
  "Delete the current file from the esp32."
  (interactive)
  (shell-command (concat "ampy -p " esp-mode-device " rm "
                         (get-relative-path buffer-file-truename)))
  )
(defun ampy-ls ()
  "Execute ampy's ls."
  (interactive)
  (shell-command (concat "ampy -p " esp-mode-device " ls "))
  )
(defun ampy-reset ()
  "Execute ampy's reset."
  (interactive)
  (shell-command (concat "ampy -p " esp-mode-device " reset "))
  )
(defun ampy-run ()
  "Execute ampy's run."
  (interactive)
  (shell-command (concat "ampy -p " esp-mode-device " run -n "
                         (projectile-project-root) esp-mode-entrypoint))
  )
(define-minor-mode esp-mode
  "group of utilities for operating on ESP32 with Adafruit's Ampy."
  :lighter "esp"
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map (kbd "C-c a p") 'ampy-put-current-buffer)
            (define-key map (kbd "C-c a R") 'ampy-rm-current-buffer)
            (define-key map (kbd "C-c a r") 'ampy-reset)
            (define-key map (kbd "C-c a e") 'ampy-run)
            (define-key map (kbd "C-c a l") 'ampy-ls)
            map))
(provide 'esp)
;;; esp.el ends here
