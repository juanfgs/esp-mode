# esp-mode

An emacs minor mode that makes easier to deploy files through Adafruit Ampy and other tools related to ESP32 programming.

Requirements
==========

This mode depends on

* Projectile mode
* Adafruit Ampy